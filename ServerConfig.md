# Server Config

**These steps are useful for debian servers**

# SSH configuration

### Requirements
1. Root access

### Steps
1. Open sshd_config file with the following command:`nano /etc/ssh/sshd_config`

2. Uncomment Port and change 22 to your desired port number.

3. Uncomment PermitRootLogin set its value to no

4. Save file and restart the sshd service by running the following command: `systemctl restart sshd` 

# Install LAMP

1. Root access
2. ### Apache

    `apt install apache2`

    You can see apache defult page on `http://your_server_ip` now!

3. ### MariaDB

    `apt install mariadb-server`

    `mysql_secure_installation`

4. ### PHP

    `apt install php libapache2-mod-php php-mysql`
    
    `nano /etc/apache2/mods-enabled/dir.conf`
    
    Move `index.php` to the start of line like this:
    
        <IfModule mod_dir.c>
            DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
        </IfModule>
        
    `systemctl restart apache2`
    
    `nano /var/www/html/info.php`
    
        <?php
        phpinfo();
        ?>
        
    Check on `http://your_server_ip/info.php`
    
    https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mariadb-php-lamp-stack-debian9

5. ### PHPMyAmin

    `apt install phpmyadmin php-mbstring php-gettext`
    
    `phpenmod mbstring`
    
    `systemctl restart apache2`
    
    Check on `https://your_domain_or_IP/phpmyadmin`


    https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-phpmyadmin-on-debian-9
    

### Possible Errors:
    
If you can't login with root user in phpmyadmin, create another user and set GRANT ALL PRIVILEGES to that:
    
`mariadb -u root -p`
    
MariDB [(none)]> `CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';`
    
MariDB [(none)]> `GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'localhost' WITH GRANT OPTION;`

MariDB [(none)]> `exit`
        
If sth went wrong and you can't understand where the problem is, don't worry! You can remove packages and start from the first :)
    
Just run these for remove a package with all it's dependencies:
    
`apt --purge remove <yourpackage>`
    
`apt --purge autoremove`

If you deal with this problem:
        
       ● mariadb.service - MariaDB database server
            Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: enabled)
        Drop-In : /etc/systemd/system/mariadb.service.d
                    migrated-from-my.cnf-setting.conf
        Active: failed (Result: timeout) since Wed 2019-06-05 00:25:28 +0430; 1h 18min ago
            Docs: man:mysqld(8)
                  https://mariadb.com/kb/en/library/systemd/
        Main PID: 5123 (code=exited, status=1/FAILURE)
            Status: "MariaDB server is down"

        Jun 05 00:25:27 marzieh-lp mysqld[4157]: 2019-06-05 0:25:27 0   [ERROR] InnoDB:Plugin initialization aborted with error Cannot open a file
        Jun 05 00:25:28 marzieh-lp mysqld[4157]: 2019-06-05 0:25:27 0   [Note] InnoDB:Starting shutdown...
        Jun 05 00:25:28 marzieh-lp mysqld[4157]: 2019-06-05 0:25:27 0   [ERROR] Plugin 'InnoDB' init function returned error.
        Jun 05 00:25:28 marzieh-lp mysqld[4157]: 2019-06-05 0:25:27 0   [ERROR] Plugin 'InnoDB' registeration as a STORAGE ENGINE failed.
        Jun 05 00:25:28 marzieh-lp mysqld[4157]: 2019-06-05 0:25:27 0   [Note] Plugin 'FEEDBACK' is disabled.
        Jun 05 00:25:28 marzieh-lp mysqld[4157]: 2019-06-05 0:25:27 0   [ERROR] Unknown/unsupported storage engine: InnoDB
        Jun 05 00:25:28 marzieh-lp mysqld[4157]: 2019-06-05 0:25:27 0   [Error] Aborting
        Jun 05 00:25:28 marzieh-lp systemd[1]: mariadb.service: Main processs exited, code=exited, status=1/FAILURE
        Jun 05 00:25:28 marzieh-lp systemd[1]: Failed to start MariaDB database server.


If you haven't any real data in your database then clear all in /var/lib/mysql with `rm -rf /var/lib/mysql`.
Then run `mysql_secure_installation`.
Now it will wokrs!


# Reverse Proxy

1. Root access
2. First of all, enable these modules with these commands:

    `a2enmod proxy`

    `a2enmod proxy_http`

    `a2enmod proxy_balancer`

    `a2enmod lbmethod_byrequests`

3. run `systemctl restart apache2`

4. `nano /etc/apache2/sites-available/000-default.conf`

        <VirtualHost *:80>
        <Proxy balancer://mycluster>
            BalancerMember http://127.0.0.1:3001
            BalancerMember http://127.0.0.1:80
        </Proxy>

            ProxyPreserveHost On

            ProxyPass / balancer://mycluster/
            ProxyPassReverse / balancer://mycluster/
        </VirtualHost>

5. `systemctl restart apache2`

https://www.digitalocean.com/community/tutorials/how-to-use-apache-as-a-reverse-proxy-with-mod_proxy-on-debian-8

# Setup DNS

### Requirements

1. Webmin

2. Root access
 
3. Bind service(`apt install bind9`)

### Steps

1. Enable bind9 service: `systemctl enable bind9`
2. Start bind9 service: `systemctl start bind9`
3. Go to your webmin page -> Server > BIND DNS Server
4. ### Creating a Master Zone

    Click on `Creating Master Zone`
    
    Enter your domain name in `Domain name / Network`
    
    Like last step, enter your domain name in ` Master Server`
    
    Click `Create NS record`
    
    Enter your mail
    
    Enter your server ip in `IP Address`
    
    Click `Create` :)
    
5. ### Creating A records (Address Records)

    Click on `Address`
    
    Keep `Name` field empty and enter your ip in `Address` field
    
    Save
    
    Enter `www` in `Name` field and your ip in `Address`
    
    Save
    
    Enter `mail` in `Name` and ip in `Address`
    
    Save
    
    Enter `ftp` in `Name` and ip in `Address`
    
    Save
    
    Enter `ns1` in `Name` and ip in `Address`
    
    Save
    
    Enter `ns2` in `Name` and ip in `Address`
    
    Save
    
    Click on `Return to Record Types`
    
6. ### Creating NS records (Name Servers)

    Click on `Name Server`
    
    Enter your domain name for `Zone Name` (Type `.` at the end of your domain name: `expteam.ir.`)
    
    Enter ns1 for `Name Server` like: `ns1.expteam.ir.` (Don't forget `.` at the end!)
    
    Save
    
    Enter ns2 for `Name Server` like: `ns2.expteam.ir.` (Don't forget `.` at the end!)
    
    Save
    
    Click on `Return to Record Types`
    
7. Apply changes by clicking on `Apply zone` at top right!
    
    Congratulation! :))
    
    http://linuxseason.ir/2016/08/%D8%AA%D9%86%D8%B8%DB%8C%D9%85-%DA%A9%D8%A7%D9%85%D9%84-dns-%D8%A8%D8%A7-webmin/
    

# Change MySQL Timeout expired (Optional)

**mysql>** `select @@global.net_write_timeout,@@global.net_read_timeout;`

    
        +----------------------------+---------------------------+
        | @@global.net_write_timeout | @@global.net_read_timeout |
        +----------------------------+---------------------------+
        |                         60 |                        30 |
        +----------------------------+---------------------------+
        1 row in set (0.00 sec)
        
    
`set @@global.net_write_timeout = 3600;`


        Query OK, 0 rows affected (0.00 sec)
    
    
`set @@global.net_read_timeout = 3600; `

        Query OK, 0 rows affected (0.00 sec)
        
        
`select @@global.net_write_timeout,@@global.net_read_timeout;`

    
        +----------------------------+---------------------------+
        | @@global.net_write_timeout | @@global.net_read_timeout |
        +----------------------------+---------------------------+
        |                       3600 |                      3600 |
        +----------------------------+---------------------------+
        1 row in set (0.00 sec)
**Attention: These changes will be reset after restarting mysql service!**

https://michlstechblog.info/blog/mysql-mariadb-timeout-expired-the-timeout-period-elapsed-prior-to-completion-of-the-operation-or-the-server-is-not-responding/

